package com.example.covid_app.info

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.covid_app.api.RetrofitClient
import com.example.covid_app.data.models.Respuesta
import com.example.covid_app.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class TestViewModel : ViewModel() {

    var date = String()
    var code = String()
    val casosConfirmados = MutableLiveData<String>()
    val casosUci = MutableLiveData<String>()
    val casosFallecidos = MutableLiveData<String>()
    val casosHospitalizados = MutableLiveData<String>()
    val casosRecuperados = MutableLiveData<String>()
    val casosConfirmadosDiario = MutableLiveData<String>()
    val casosUciDiario = MutableLiveData<String>()
    val casosFallecidosDiario = MutableLiveData<String>()
    val casosHospitalizadosDiario = MutableLiveData<String>()
    val casosRecuperadosDiario = MutableLiveData<String>()


    init {
        getTodayCom()
    }

    fun getTodayCom() {
        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                RetrofitClient().getTodayCom(date, code)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseRegiones: Respuesta = response.body() as Respuesta
                            casosConfirmados.value =
                                responseRegiones.timeline[0].regiones[0].data.casosConfirmados.toString()
                            casosUci.value =
                                responseRegiones.timeline[0].regiones[0].data.casosUci.toString()
                            casosFallecidos.value =
                                responseRegiones.timeline[0].regiones[0].data.casosFallecidos.toString()
                            casosHospitalizados.value =
                                responseRegiones.timeline[0].regiones[0].data.casosHospitalizados.toString()
                            casosRecuperados.value =
                                responseRegiones.timeline[0].regiones[0].data.casosRecuperados.toString()
                            casosConfirmadosDiario.value =
                                responseRegiones.timeline[0].regiones[0].data.casosConfirmadosDiario.toString()
                            casosUciDiario.value =
                                responseRegiones.timeline[0].regiones[0].data.casosUciDiario.toString()
                            casosFallecidosDiario.value =
                                responseRegiones.timeline[0].regiones[0].data.casosFallecidosDiario.toString()
                            casosHospitalizadosDiario.value =
                                responseRegiones.timeline[0].regiones[0].data.casosHospitalizadosDiario.toString()
                            casosRecuperadosDiario.value =
                                responseRegiones.timeline[0].regiones[0].data.casosRecuperadosDiario.toString()

                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }

}