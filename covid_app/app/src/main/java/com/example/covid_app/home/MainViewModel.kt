package com.example.covid_app.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.covid_app.api.RetrofitClient
import com.example.covid_app.data.models.Respuesta
import com.example.covid_app.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class MainViewModel : ViewModel() {

    val latitude: MutableList<String> = ArrayList()
    val longitude: MutableList<String> = ArrayList()
    val getAll = MutableLiveData<Boolean>()



    init {
        getAll()
    }

    fun getAll() {
        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                RetrofitClient().getAll()
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseAll: Respuesta = response.body() as Respuesta
                            for(i in 0..18){
                                latitude.add(responseAll.timeline[0].regiones[i].lat.toString())
                                longitude.add(responseAll.timeline[0].regiones[i].long.toString())
                            }
                            getAll.value = true
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }
}