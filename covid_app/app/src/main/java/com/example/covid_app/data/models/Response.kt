package com.example.covid_app.data.models

data class Respuesta (
    val id: String,
    val trace: Trace,
    val timeline: List<Timeline>
)

data class Timeline (
    val regiones: List<Regione>,
    val fecha: String,
    val fechaInicio: String,
    val fechaFin: String,
    val numeroFechas: Long,
    val periodo: Periodo
)

enum class Periodo {
    Dia
}

data class Regione (
    val nivelAdministrativo: NivelAdministrativo,
    val nombreLugar: NombreLugar,
    val codigoISOLugar: CodigoISOLugar,
    val long: Double,
    val lat: Double,
    val data: Data
)

enum class CodigoISOLugar {
    EsAn,
    EsAr,
    EsAs,
    EsCM,
    EsCN,
    EsCT,
    EsCb,
    EsCe,
    EsCl,
    EsEx,
    EsGa,
    EsIb,
    EsMc,
    EsMd,
    EsMl,
    EsNc,
    EsPV,
    EsRi,
    EsVc
}

data class Data (
    val id: String,
    val casosConfirmados: Long,
    val casosUci: Long,
    val casosFallecidos: Long,
    val casosHospitalizados: Long,
    val casosRecuperados: Long,
    val casosConfirmadosDiario: Long,
    val casosUciDiario: Long,
    val casosFallecidosDiario: Long,
    val casosHospitalizadosDiario: Long,
    val casosRecuperadosDiario: Long
)

enum class NivelAdministrativo {
    Ccaa
}

enum class NombreLugar {
    Andalucía,
    Aragón,
    Asturias,
    Baleares,
    Canarias,
    Cantabria,
    CastillaLaMancha,
    CastillaYLeón,
    Cataluña,
    Ceuta,
    ComunidadValenciana,
    Extremadura,
    Galicia,
    LaRioja,
    Madrid,
    Melilla,
    Murcia,
    Navarra,
    PaisVasco
}

data class Trace (
    val id: String,
    val info: Info,
    val contacto: Contacto
)

data class Contacto (
    val id: String,
    val nombreEmpresa: String,
    val url: String,
    val email: String
)

data class Info (
    val id: String,
    val nombre: String,
    val creado: String,
    val timestamp: Long,
    val fecha: String,
    val fechaInicio: String,
    val fechaFin: String,
    val numeroFechas: Long,
    val periodo: Periodo,
    val nivelAdministrativo: NivelAdministrativo,
    val nombreLugar: String,
    val codigoISOLugar: String,
    val numeroLugares: Long
)