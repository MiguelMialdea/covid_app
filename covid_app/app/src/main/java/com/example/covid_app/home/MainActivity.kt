package com.example.covid_app.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.covid_app.R
import com.example.covid_app.data.models.CodigoISOLugar
import com.example.covid_app.data.models.NombreLugar
import com.example.covid_app.databinding.ActivityMainBinding
import com.example.covid_app.info.TestActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    private lateinit var finalMonth: String
    private lateinit var finalDay: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        binding.lifecycleOwner = this
        binding.mainViewModel = viewModel
        binding.mainActivity = this

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.google_map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        binding.textFecha.setOnClickListener { showDatePickerDialog() }
        spinnerCode()

    }


    private fun showDatePickerDialog() {
        val datePicker =
            DatePickerFragment({ day, month, year -> onDateSelected(day, month, year) })
        datePicker.show(supportFragmentManager, "datePicker")
    }

    fun onDateSelected(day: Int, month: Int, year: Int) {
        if (month < 10) {
            finalMonth = "0$month"
        } else {
            finalMonth = "$month"
        }
        if (day < 10) {
            finalDay = "0$day"
        } else {
            finalDay = "$day"
        }
        binding.textFecha.setText("$year-" + finalMonth + "-" + finalDay)
        binding.buttonIntent.isEnabled = true

    }

    private fun spinnerCode() {
        val list = NombreLugar.values()

        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, list)

        binding.spinner.adapter = adapter

        binding.spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val word = CodigoISOLugar.values().get(position).toString()

                val firstLetters = word[0].toString() + word[1].toString()
                val secondLetters = word[2].toString() + word[3].toString()
                binding.textCodigo.text = firstLetters + "-" + secondLetters

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        viewModel.getAll.observe(this, Observer {
            if (it) {
                for (i in 0..18) {
                    val coords = LatLng(
                        viewModel.latitude.get(i).toDouble(),
                        viewModel.longitude.get(i).toDouble()
                    )
                    mMap.addMarker(MarkerOptions().position(coords).title(coords.toString()))
                    mMap.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(
                                viewModel.latitude.get(15).toDouble(),
                                viewModel.longitude.get(15).toDouble()
                            ), 5f
                        ),
                        4000,
                        null
                    )
                }


                binding.buttonIntent.setOnClickListener {
                    val code = binding.textCodigo.text.toString()
                    val date = binding.textFecha.text.toString()
                    val city = binding.spinner.selectedItem.toString()

                    val intent = Intent(this@MainActivity, TestActivity::class.java)
                    intent.putExtra("Codigo", code)
                    intent.putExtra("Date", date)
                    intent.putExtra("City", city)
                    startActivity(intent)
                }
            }
        })

    }

}