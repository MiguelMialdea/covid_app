package com.example.covid_app.info

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.covid_app.R
import com.example.covid_app.databinding.ActivityTestBinding

class TestActivity : AppCompatActivity(){

    private lateinit var binding: ActivityTestBinding
    private lateinit var viewModel: TestViewModel
    private lateinit var code: String
    private lateinit var date: String
    private lateinit var city: String



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_test
        )
        viewModel = ViewModelProvider(this).get(TestViewModel::class.java)
        binding.lifecycleOwner = this
        binding.testViewModel = viewModel
        binding.testActivity = this

        code = intent.getStringExtra("Codigo").toString()
        date = intent.getStringExtra("Date").toString()
        city = intent.getStringExtra("City").toString()


        binding.textCode.text = city
        binding.textDate.text = date
        viewModel.date = date
        viewModel.code = code
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}