package com.example.covid_app.api

import com.example.covid_app.data.models.Respuesta
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    //GET ALL
    @GET("es/ccaa")
    suspend fun getAll(): Response<Respuesta>

    //GET TODAY TCOMMUNIY INFO
    @GET("es/ccaa")
    suspend fun getTodayCom(
        @Query("fecha") fecha: String?,
        @Query("codigo") codigo: String?
    ): Response<Respuesta>

    /*//GET LAST DAYS COMMUNITY INFO
    @GET("es/ccaa")
    suspend fun getLastDaysCom(
        @Query("ultimodia") ultimodia: Boolean?,
        @Query("codigo") codigo: String?
    ): Response<Regiones>*/
}